import { Request, Response } from 'express';
import { Team } from '../models/teamModel';
import { Click } from '../models/clickModel';
import { customers } from './customerController';

let teams: Team[] = [];
let clicks: Click[] = [];

// GET the clicks log (history)
export const fetchClicks = (req: Request, res: Response) => {
    return res.json(clicks);
};

// GET sums for all teams
export const fetchLeaderboard = (req: Request, res: Response) => {
    return res.json(teams);
};

// GET a single team info
export const fetchSingleTeam = (req: Request, res: Response) => {
    const { name } = req.params;
    const team = teams.find((t) => t.name === name);

    if (!team) {
        return res.status(404).json({ error: 'Team not found' });
    }

    return res.json(team);
};

// POST a new click associated to selected team
export const addClickToTeam = (req: Request, res: Response) => {
    const { teamName, customerToken }: { teamName: string, customerToken: string } = req.body;

    if (!teamName) {
        return res.status(400).json({ error: 'Team name is required' });
    }

    if (!customerToken) {
        return res.status(400).json({ error: 'Customer token is required' });
    }

    // Check whether the selected team exists
    let team = teams.find((t) => t.name === teamName);

    // If the team does not exist, create a new one
    if (!team) {
        team = {
            name: teamName,
            clicks: 0
        };
        teams.push(team);
    }

    // Check the customer existance
    const customer = customers.find((c) => c.token === customerToken);

    if (!customer) {
        return res.status(404).json({ error: 'Customer not found' });
    }

    // Increment the team click counter
    team.clicks += 1;

    // Create new click row in click history (with current clicks count for selected team)
    const click = {
        team: { ...team },
        customer,
        created: new Date()
    };
    clicks.push(click);

    return res.status(201).json(team);
};
