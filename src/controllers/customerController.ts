import { Request, Response } from 'express';
import { Customer } from '../models/customerModel';

export let customers: Customer[] = [];

// POST register new customer
export const registerCustomer = (req: Request, res: Response) => {
    const { email }: { email: string } = req.body;

    if (!email) {
        return res.status(400).json({ error: 'Email is required' });
    }

    // Basic dummy email validation
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailRegex.test(email)) {
        return res.status(400).json({ error: 'Invalid email format' });
    }

    // Check if the email is already registered - if so, return Bad request
    if (customers.some((customer) => customer.email === email.toLowerCase())) {
        return res.status(400).json({ error: 'Email already registered' });
    }

    // Generate pseudo-token
    let hash = '';
    do {
        hash = Array.from({ length: 6 }, () => Math.random().toString(36).charAt(2)).join('');
    } while (customers.some((customer) => customer.token === hash));

    // Save newCustomer to in-memory customers array
    const newCustomer: Customer = { email: email.toLowerCase(), token: hash};
    customers.push(newCustomer);

    return res.status(201).json(newCustomer);
};

// POST login existing customer
export const loginCustomer = (req: Request, res: Response) => {
    const { email }: { email: string } = req.body;

    if (!email) {
        return res.status(400).json({ error: 'Email is required' });
    }

    // Check if the email is already registered
    const customer = customers.find((c) => c.email === email.toLowerCase());

    if (!customer) {
        return res.status(401).json({ error: 'Invalid email' });
    }

    return res.json(customer);
};
