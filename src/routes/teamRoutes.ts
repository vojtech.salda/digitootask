import express, { Router } from 'express';
import { fetchLeaderboard, fetchSingleTeam, addClickToTeam, fetchClicks } from '../controllers/teamController';

const router: Router = express.Router();

router.get('/clicks-history', fetchClicks);
router.get('/leaderboard', fetchLeaderboard);
router.get('/team/:name', fetchSingleTeam);
router.post('/click', addClickToTeam);

export default router;
