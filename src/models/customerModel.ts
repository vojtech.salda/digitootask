export interface Customer {
    email: string;
    token: string;
}