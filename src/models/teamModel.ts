export interface Team {
    name: string;
    clicks: number;
}