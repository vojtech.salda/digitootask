import { Customer } from './customerModel';
import { Team } from './teamModel';

export interface Click {
    customer: Customer;
    team: Team;
    created: Date
}
