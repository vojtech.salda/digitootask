import express from 'express';
import bodyParser from 'body-parser';
import customerRoutes from './routes/customerRoutes';
import teamRoutes from './routes/teamRoutes';

const app = express();
app.use(bodyParser.json());

app.use('/customers', customerRoutes);
app.use('/teams', teamRoutes);

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});
